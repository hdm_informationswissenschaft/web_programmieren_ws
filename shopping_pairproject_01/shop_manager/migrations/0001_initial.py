# Generated by Django 3.2.5 on 2022-10-30 14:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('shop_name', models.CharField(max_length=50)),
                ('opening_hours', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item_name', models.CharField(max_length=50)),
                ('status', models.BooleanField(default=False)),
                ('amount_float', models.FloatField(default=1)),
                ('amount_type', models.CharField(choices=[('L', 'Liter'), ('KG', 'Kilogramm'), ('S', 'Stückzahl')], default='S', max_length=2)),
                ('shop', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shop_manager.shop')),
            ],
        ),
    ]
