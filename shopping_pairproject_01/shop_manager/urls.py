"""shopping_pairproject_01 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'shop_manager'
urlpatterns = [
    path('', views.index, name='index'),
    path('add_shop', views.add_shop, name='add_shop'),
    path('delete_shop/<int:shop_id>', views.delete_shop, name='delete_shop'),
    path('shopping_list_detail/<int:list_id>', views.shopping_list_detail, name='shopping_list_detail'),
    path('add_shopping_list', views.add_shopping_list, name='add_shopping_list'),
    path('delete_shopping_list/<int:list_id>', views.delete_shopping_list, name='delete_shopping_list'),
    path('add_item/<int:list_id>', views.add_item, name='add_item'),
    path('delete_item/<int:list_id>/<int:item_id>', views.delete_item, name='delete_item'),
    path('change_status/<int:list_id>/<int:item_id>', views.change_status, name='change_status'),
    path('shop_detail/<int:shop_id>', views.shop_detail, name='shop_detail'),
]



