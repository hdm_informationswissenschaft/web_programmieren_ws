from django.contrib import admin
from . import models
# Register your models here.
admin.site.register(models.Item)
admin.site.register(models.Shop)
admin.site.register(models.Shopping_list)