from django.db import models
import datetime
from datetime import date
#from django.utils.timezone import now
# Create your models here.

item_type = [
    ('L', 'Liter'),
    ('KG', 'Kilogramm'),
    ('S', 'Stückzahl'),
]



class Item(models.Model):
    item_name = models.CharField(max_length=50)
    status = models.BooleanField(default = False)
    amount_float = models.FloatField(default=1)
    amount_type = models.CharField(max_length=2, choices=item_type, default="S")
    shop = models.ForeignKey(to="Shop", on_delete=models.CASCADE)
    #list = models.ForeignKey(to="Shopping_list", on_delete=models.SET_NULL, null=True)
    list = models.ForeignKey(to="Shopping_list", on_delete=models.CASCADE, default=1)
    

    def type_translation(self):
        type_text = {
            "L": "Liter",
            "KG": "Kilogramm",
            "S": "Stückzahl",
        }
        return type_text[self.amount_type]
    
    def __str__(self):
        return f'{self.item_name} {self.amount_type[0]}'



class Shop(models.Model):
    shop_name = models.CharField(max_length=50)
    opening_hours = models.CharField(max_length=50)

    def __str__(self):
        return self.shop_name


class Shopping_list(models.Model):
    list_name = models.CharField(max_length=50, default= "shop")
    shopping_date = models.DateField(auto_now_add=False, auto_now=False, default=date.today)
    list_date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.list_name
