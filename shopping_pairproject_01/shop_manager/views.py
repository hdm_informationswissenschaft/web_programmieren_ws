from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import Shop, Item, Shopping_list
from django.urls import reverse
# Create your views here.

def index(request):
    context = {}
    context["shopping_lists"]=Shopping_list.objects.all()
    context["shops"]=Shop.objects.all()
    return render(request, "shop_manager/index.html", context)


def add_shop(request):
    if "shop_name" in request.POST:
        shop = Shop()
        shop.shop_name = request.POST["shop_name"]
        shop.opening_hours = request.POST["opening_hours"]
        shop.save()
    return redirect("shop_manager:index")

def delete_shop(request, shop_id):
    shop = Shop.objects.get(pk=shop_id)
    shop.delete()
    return redirect("shop_manager:index")


def shopping_list_detail(request, list_id):
    context = {}
    context["items"]=Item.objects.filter(list=list_id)
    context["shopping_list"]=Shopping_list.objects.get(pk=list_id)
    context["shops"]=Shop.objects.all()
    return render(request, "shop_manager/shopping_list_detail.html", context)

def add_shopping_list(request):
    if "list_name" in request.POST:
        shopping_list = Shopping_list()
        shopping_list.list_name = request.POST["list_name"]
        shopping_list.shopping_date = request.POST["shopping_date"]
        shopping_list.save()
    return redirect("shop_manager:index")

def delete_shopping_list(request, list_id):
    shopping_list = Shopping_list.objects.get(pk=list_id)
    shopping_list.delete()
    return redirect("shop_manager:index")

def add_item(request, list_id):
    list = Shopping_list.objects.get(pk=list_id)
    if "item_name" in request.POST:
        item = Item()
        item.item_name = request.POST["item_name"]
        item.amount_float = request.POST["amount_float"]
        item.amount_type = request.POST["amount_type"]
        item.shop = Shop.objects.get(pk=request.POST["shop_name"])
        item.list = Shopping_list.objects.get(pk=list_id)
        item.save()
    return HttpResponseRedirect(reverse("shop_manager:shopping_list_detail", args=(list.id,)))

def delete_item(request, item_id, list_id):
    list = Shopping_list.objects.get(pk=list_id)
    item = Item.objects.get(pk=item_id)
    item.delete()
    return HttpResponseRedirect(reverse("shop_manager:shopping_list_detail", args=(list.id,)))

def change_status(request, item_id, list_id):
    list = Shopping_list.objects.get(pk=list_id)
    item = Item.objects.get(pk=item_id)
    item.status = True
    item.save()
    return HttpResponseRedirect(reverse("shop_manager:shopping_list_detail", args=(list.id,)))


def shop_detail(request, shop_id):
    context = {}
    context["items"]=Item.objects.filter(shop=shop_id)
    context["shops"]=Shop.objects.get(pk=shop_id)
    return render(request, "shop_manager/shop_detail.html", context)