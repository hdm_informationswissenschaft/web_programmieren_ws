from django.db import models

# Create your models here.

task_status = [
    ('T', 'Todo'),
    ('D', 'Done'),
    ('C', 'Canceled'),
]

class Task(models.Model):
    text = models.CharField(max_length=200)
    status = models.CharField(max_length=1, choices=task_status, default="T")

    def task_translation(self):
        task_texts = {
            "T": "Todo",
            "D": "Erledigt",
            "C": "Gecancelt",
            
        }
        return task_texts[self.status]

    def __str__(self):
        return f"{self.text} {self.status}"