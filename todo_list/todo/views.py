from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import models
# Create your views here.

def index(request):
    task_list = models.Task.objects.all()
    context = {"task_list" : task_list}
    
    return render(request, "todo/index.html", context)

def add_task(request):
    if "text" in request.POST:
        task = models.Task()
        task.text= request.POST["text"]
        task.save()
    return redirect("index")
#def change_task_status(request):



#def delete_task(request):

# Chaos Test